from docxtpl import DocxTemplate
import xlrd
import os
import time


choise = int(input("Какой сертификат нужен?\n1-базовый \n2-проектный \n"))
if choise == 1:
    loc = os.path.abspath("uch_base.xlsx")
elif choise == 2:
    loc = os.path.abspath("uch_proj.xlsx")
    
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)

time.sleep(0.5)
print("\nСоздаются сертификаты. Ждите...")

for rownum in range(sheet.nrows):
    if choise == 1:
        doc = DocxTemplate(os.path.abspath("s_base.docx"))
        sType = "Certificates_Base"
    elif choise == 2:
        doc = DocxTemplate(os.path.abspath("s_proj.docx"))
        sType = "Certificates_Project"
    row = sheet.row_values(rownum)
    fio = str(row[0])
    prog = str(row[1])
    prof = str(row[2])
    context = {'fio' : fio, 'prog' : prog, 'prof' : prof}
    doc.render(context)
    doc.save("./"+sType+"/sert_" + fio + ".docx")

input("Нажмите Enter для выхода")
